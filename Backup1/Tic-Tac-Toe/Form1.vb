﻿Public Class TicTacToe
    ' These booleans will help us determine if a certain
    ' box is already taken. It would be more efficient
    ' to use an array, but we do not know how, so this
    ' will have to do for now.
    Dim spotOneGuessed As Boolean = False
    Dim spotTwoGuessed As Boolean = False
    Dim spotThreeGuessed As Boolean = False
    Dim spotFourGuessed As Boolean = False
    Dim spotFiveGuessed As Boolean = False
    Dim spotSixGuessed As Boolean = False
    Dim spotSevenGuessed As Boolean = False
    Dim spotEightGuessed As Boolean = False
    Dim spotNineGuessed As Boolean = False
    Dim randomizer As Integer = 0 'For the computer to guess.
    Dim availabiltyChecked As Boolean = False 'To make sure the
    'computer doesn't override any user guesses, this variable
    'is toggled back and forth to trigger a do...while loop
    'that will make certain no overriding happens.
    Dim guessCount As Integer = 0 'This keeps track of the
    'number of guesses that have been made in total.
    Dim playerMark As String = "X" 'Does the player use X or O?
    Dim computerMark As String = "O" 'Does the computer use X or O?
    Dim winnerDetermined As Boolean = False 'Has the winner been
    'determined yet?
    Dim computerGuessRoute As Integer = 0 'Determines which route the computer
    'wants to take to make the guess: random or logical. The guess will be random
    'until at least 3 total guesses have been made.
    Dim computerLogicAlreadyUsed As Boolean = False 'Determines whether or not the computer,
    'when using the logic method to determine where to place its mark, has already found a
    'suitable location, and used it.
    Dim settingsTxtFilepath As String = "C:\ProgrammersWorldProgramFiles\Tic-Tac-Toe\TicTacToeSettings.txt" 'Stores
    'the filepath of the TicTacToeSettings.txt file.
    'The next variable is for writing to the settings text file.
    Dim objFile
    Dim highlightWinningTicTacToe As Boolean = True 'Holds the value for whether or not
    'the winning 3-In-A-Row for the Tic-Tac-Toe game will be highlighted.
    Dim colorToHighlightWinningTicTacToeWith As Color = Color.Red 'This will hold the color
    'that will be used when highlighting the winning 3-In-A-Row.

    ' Upon the initial start-up of the computer, there are some things that
    ' should be initially done, this takes care of it.
    Private Sub TicTacToe_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Determine whether or not Tic-Tac-Toe has a settings text file, if it does not,
        'then make it, and populate it with some initial data. If it does, then read the
        'data, and use the data to run the program.

        'Does the standard "ProgrammersWorldProgramFiles" that all of our programs use exist? If not, then
        'let's create it.
        If Not System.IO.File.Exists("C:\ProgrammersWorldProgramFiles") Then
            My.Computer.FileSystem.CreateDirectory("C:\ProgrammersWorldProgramFiles")
        End If

        'Before we make a new file, let's see if we have an old Tic-Tac-Toe v3.0 Settings File
        'which we can import instead.
        If System.IO.File.Exists("C:\Program Files\Tic-Tac-Toe\TicTacToeSettings.txt") Then
            'If the computer makes it this far, that means that there is a v3.0 Settings File.
            'First, let's add the new directory location.
            My.Computer.FileSystem.CreateDirectory("C:\ProgrammersWorldProgramFiles\Tic-Tac-Toe")
            'Now, we are going to move the text file to the new location.
            System.IO.File.Move("C:\Program Files\Tic-Tac-Toe\TicTacToeSettings.txt", _
                                "C:\ProgrammersWorldProgramFiles\Tic-Tac-Toe\TicTacToeSettings.txt")
            My.Computer.FileSystem.DeleteDirectory("C:\Program Files\Tic-Tac-Toe", _
                                                   FileIO.DeleteDirectoryOption.ThrowIfDirectoryNonEmpty)
            'Programmers Note: You may notice that in this if statement, the program creates a new text
            'directory, then moves the text file, then deletes the old directory, rather than just moving
            'the whole directory. This is because, for some reason, when the whole old directory is moved
            'to the new directory all at once, the text file gets lost. The method above seems to work
            'properly, but for some reason, using the 'MoveDirectory' method causes the text file to get
            'lost.
        End If

        If Not System.IO.File.Exists(settingsTxtFilepath) Then
            'Since you are in here, it means that the file does not exist, so let's make it!
            My.Computer.FileSystem.CreateDirectory("C:\ProgrammersWorldProgramFiles\Tic-Tac-Toe")
            objFile = System.IO.File.Create(settingsTxtFilepath)
            objFile.Close()
            'Now that the file has been made, let's populate it with some initial data. (Default Program Setting Data)
            writeSettingsToFile()
        Else
            'Since the file already exists, we can use it to restore previous program
            'settings. Let's do it! To make sure that the text file contents were written by the
            'same version of Tic-Tac-Toe, let's use the first line, which states the version, to
            'verify it.
            Dim checkTextFileVersion As IO.StreamReader = IO.File.OpenText(settingsTxtFilepath)
            If Not checkTextFileVersion.ReadLine = "Tic-Tac-Toe v4.0 Settings File -- Do Not Modify" Then
                checkTextFileVersion.Close()
                readSettingsFromFile()
                writeSettingsToFile()
            Else
                checkTextFileVersion.Close()
                readSettingsFromFile()
            End If
        End If
        matchSettingsToVariables()
    End Sub

    'This will make all elements of the Tic-Tac-Toe game visible.

    'This takes the program settings, and writes them to a text file so that
    'they can be reused each time the program is run.
    Sub writeSettingsToFile()
        'Clear out all of the current data in the text file, and write the header of the file back in.
        System.IO.File.WriteAllText(settingsTxtFilepath, "Tic-Tac-Toe v4.0 Settings File -- Do Not Modify" & vbCrLf)
        'Open the file for writing.
        objFile = System.IO.File.AppendText(settingsTxtFilepath)
        'Write the values of various variables into the text file.
        objFile.WriteLine(playerMark)
        objFile.WriteLine(computerMark)
        objFile.WriteLine(highlightWinningTicTacToe)
        objFile.WriteLine(colorToHighlightWinningTicTacToeWith)
        'All of the data has been written to the text file at this point. Now, close
        'the file.
        objFile.Close()
    End Sub

    'This takes the text file, and reads it into the program settings so that
    'the data in the file is used in the program.
    Sub readSettingsFromFile()
        'Open the text file for reading.
        Dim settingsTextFile As IO.StreamReader = IO.File.OpenText(settingsTxtFilepath)
        'Read the text file.
        settingsTextFile.ReadLine() 'This line has the file information on it that we want to ignore, but to bypass this line, we must call it, so I did.
        playerMark = settingsTextFile.ReadLine
        computerMark = settingsTextFile.ReadLine
        If settingsTextFile.ReadLine = "True" Then
            highlightWinningTicTacToe = True
        Else
            highlightWinningTicTacToe = "False"
        End If
        Dim colorDeterminerForWinning3InARow As String = settingsTextFile.ReadLine
        If colorDeterminerForWinning3InARow = "Color [Red]" Then
            colorToHighlightWinningTicTacToeWith = Color.Red
        ElseIf colorDeterminerForWinning3InARow = "Color [Orange]" Then
            colorToHighlightWinningTicTacToeWith = Color.Orange
        ElseIf colorDeterminerForWinning3InARow = "Color [Yellow]" Then
            colorToHighlightWinningTicTacToeWith = Color.Yellow
        ElseIf colorDeterminerForWinning3InARow = "Color [Green]" Then
            colorToHighlightWinningTicTacToeWith = Color.Green
        ElseIf colorDeterminerForWinning3InARow = "Color [Blue]" Then
            colorToHighlightWinningTicTacToeWith = Color.Blue
        ElseIf colorDeterminerForWinning3InARow = "Color [Purple]" Then
            colorToHighlightWinningTicTacToeWith = Color.Purple
        ElseIf colorDeterminerForWinning3InARow = "Color [Pink]" Then
            colorToHighlightWinningTicTacToeWith = Color.Pink
        End If
        'Close the text file.
        settingsTextFile.Close()
    End Sub

    ' This is done when the computer takes it's turn.
    Sub computerGuess()
        'Has a winner been determined yet? If not, then
        'let the computer choose another spot. Otherwise,
        'skip over this entire function.
        If winnerDetermined = False Then
            'Determine whether the computer is going to take the
            'random route or the logical route. This is not going
            'to matter until at least 3 guesses have been made.
            computerGuessRoute = Int(Rnd() * 2) + 1

            'If 'computerGuessRoute' equals 1, then take the logical route,
            'but only if 'guessCount' is greater than or equal to 3. If
            ' 'computerGuessRoute' equals 2, then the computer will take
            'the random route.
            If computerGuessRoute = 1 And guessCount >= 3 Then
                If spotOne.Text = playerMark And computerLogicAlreadyUsed = False Then
                    If spotTwo.Text = playerMark And spotThreeGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotThreeGuessed = False Then
                            spotThreeGuessed = True
                            spotThree.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotThree.Text = playerMark And spotTwoGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotTwoGuessed = False Then
                            spotTwoGuessed = True
                            spotTwo.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotFour.Text = playerMark And spotSevenGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotSevenGuessed = False Then
                            spotSevenGuessed = True
                            spotSeven.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotSeven.Text = playerMark And spotFourGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotFourGuessed = False Then
                            spotFourGuessed = True
                            spotFour.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotFive.Text = playerMark And spotNineGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotNineGuessed = False Then
                            spotNineGuessed = True
                            spotNine.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotNine.Text = playerMark And spotFiveGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotFiveGuessed = False Then
                            spotFiveGuessed = True
                            spotFive.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                End If
                If spotTwo.Text = playerMark And computerLogicAlreadyUsed = False Then
                    If spotThree.Text = playerMark And spotOneGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotOneGuessed = False Then
                            spotOneGuessed = True
                            spotOne.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotFive.Text = playerMark And spotEightGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotEightGuessed = False Then
                            spotEightGuessed = True
                            spotEight.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotEight.Text = playerMark And spotFiveGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotFiveGuessed = False Then
                            spotFiveGuessed = True
                            spotFive.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                End If
                If spotThree.Text = playerMark And computerLogicAlreadyUsed = False Then
                    If spotSix.Text = playerMark And spotNineGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotNineGuessed = False Then
                            spotNineGuessed = True
                            spotNine.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotNine.Text = playerMark And spotSixGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotSixGuessed = False Then
                            spotSixGuessed = True
                            spotSix.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotFive.Text = playerMark And spotSevenGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotSevenGuessed = False Then
                            spotSevenGuessed = True
                            spotSeven.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotSeven.Text = playerMark And spotFiveGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotFiveGuessed = False Then
                            spotFiveGuessed = True
                            spotFive.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                End If
                If spotFour.Text = playerMark And computerLogicAlreadyUsed = False Then
                    If spotFive.Text = playerMark And spotSixGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotSixGuessed = False Then
                            spotSixGuessed = True
                            spotSix.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotSix.Text = playerMark And spotFiveGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotFiveGuessed = False Then
                            spotFiveGuessed = True
                            spotFive.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotSeven.Text = playerMark And spotOneGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotOneGuessed = False Then
                            spotOneGuessed = True
                            spotOne.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                End If
                If spotFive.Text = playerMark And computerLogicAlreadyUsed = False Then
                    If spotNine.Text = playerMark And spotOneGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotOneGuessed = False Then
                            spotOneGuessed = True
                            spotOne.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotEight.Text = playerMark And spotTwoGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotTwoGuessed = False Then
                            spotTwoGuessed = True
                            spotTwo.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotSeven.Text = playerMark And spotThreeGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotSevenGuessed = False Then
                            spotThreeGuessed = True
                            spotThree.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotSix.Text = playerMark And spotFourGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotFourGuessed = False Then
                            spotFourGuessed = True
                            spotFour.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                End If
                If spotSix.Text = playerMark And spotThreeGuessed = False And computerLogicAlreadyUsed = False Then
                    If spotNine.Text = playerMark And spotThreeGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotThreeGuessed = False Then
                            spotThreeGuessed = True
                            spotThree.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                End If
                If spotSeven.Text = playerMark And computerLogicAlreadyUsed = False Then
                    If spotEight.Text = playerMark And spotNineGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotNineGuessed = False Then
                            spotNineGuessed = True
                            spotNine.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                    If spotNine.Text = playerMark And spotEightGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotEightGuessed = False Then
                            spotEightGuessed = True
                            spotEight.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                End If
                If spotEight.Text = playerMark And computerLogicAlreadyUsed = False Then
                    If spotNine.Text = playerMark And spotSevenGuessed = False And computerLogicAlreadyUsed = False Then
                        If spotSevenGuessed = False Then
                            spotSevenGuessed = True
                            spotSeven.Text = computerMark
                            computerLogicAlreadyUsed = True
                            guessCount += 1
                        End If
                    End If
                End If
            End If

            'There is a chance that the computer will choose to go the logical route,
            'and not find anything, and therefore, will need to use the random route.
            'To make sure that the computer is guranteed a guess, the logical route has
            'been placed in a seperate if statement.
            If computerLogicAlreadyUsed = False Or computerGuessRoute = 2 Or guessCount < 3 Then
                ' The computer decides where to to
                ' place its mark here, but we do
                ' not want the computer overriding
                ' an already taken spot, so we run
                ' in a do-while loop with if statements
                ' to check to see if the chosen spot
                ' is available if not, then go back
                ' we go back and choose another spot.
                ' We keep doing this until an open spot
                ' is found.
                Do
                    ' Choose a spot...
                    randomizer = Int(Rnd() * 9) + 1
                    ' ...now find out it if it is available.
                    If randomizer = 1 Then
                        If spotOneGuessed = False Then
                            availabiltyChecked = True
                        End If
                    ElseIf randomizer = 2 Then
                        If spotTwoGuessed = False Then
                            availabiltyChecked = True
                        End If
                    ElseIf randomizer = 3 Then
                        If spotThreeGuessed = False Then
                            availabiltyChecked = True
                        End If
                    ElseIf randomizer = 4 Then
                        If spotFourGuessed = False Then
                            availabiltyChecked = True
                        End If
                    ElseIf randomizer = 5 Then
                        If spotFiveGuessed = False Then
                            availabiltyChecked = True
                        End If
                    ElseIf randomizer = 6 Then
                        If spotSixGuessed = False Then
                            availabiltyChecked = True
                        End If
                    ElseIf randomizer = 7 Then
                        If spotSevenGuessed = False Then
                            availabiltyChecked = True
                        End If
                    ElseIf randomizer = 8 Then
                        If spotEightGuessed = False Then
                            availabiltyChecked = True
                        End If
                    ElseIf randomizer = 9 Then
                        If spotNineGuessed = False Then
                            availabiltyChecked = True
                        End If
                    End If
                Loop While availabiltyChecked = False
                availabiltyChecked = False

                ' Okay, at this point the computer has
                ' selected an available spot, and will use it!
                If randomizer = 1 Then
                    If spotOneGuessed = False Then
                        spotOne.Text = computerMark
                        guessCount += 1
                        spotOneGuessed = True
                        checkForWin()
                    End If
                ElseIf randomizer = 2 Then
                    If spotTwoGuessed = False Then
                        spotTwo.Text = computerMark
                        guessCount += 1
                        spotTwoGuessed = True
                        checkForWin()
                    End If
                ElseIf randomizer = 3 Then
                    If spotThreeGuessed = False Then
                        spotThree.Text = computerMark
                        guessCount += 1
                        spotThreeGuessed = True
                        checkForWin()
                    End If
                ElseIf randomizer = 4 Then
                    If spotFourGuessed = False Then
                        spotFour.Text = computerMark
                        guessCount += 1
                        checkForWin()
                    End If
                ElseIf randomizer = 5 Then
                    If spotFiveGuessed = False Then
                        spotFive.Text = computerMark
                        guessCount += 1
                        spotFiveGuessed = True
                        checkForWin()
                    End If
                ElseIf randomizer = 6 Then
                    If spotSixGuessed = False Then
                        spotSix.Text = computerMark
                        guessCount += 1
                        spotSixGuessed = True
                        checkForWin()
                    End If
                ElseIf randomizer = 7 Then
                    If spotSevenGuessed = False Then
                        spotSeven.Text = computerMark
                        guessCount += 1
                        spotSevenGuessed = True
                        checkForWin()
                    End If
                ElseIf randomizer = 8 Then
                    If spotEightGuessed = False Then
                        spotEight.Text = computerMark
                        guessCount += 1
                        spotEightGuessed = True
                        checkForWin()
                    End If
                ElseIf randomizer = 9 Then
                    If spotNineGuessed = False Then
                        spotNine.Text = computerMark
                        guessCount += 1
                        spotNineGuessed = True
                        checkForWin()
                    End If
                End If
            End If
        End If
        'To make sure that the variable is ready the next time that it is
        'to be used, we reset it to 'False.'
        computerLogicAlreadyUsed = False
    End Sub

    'This runs various checks to see if there is a winner.
    'No checks will be run until at least 5 guesses have been
    'made. (No checks until at least 5 guesses because if you
    'have less than 5, there is no way to have a winner, so why
    'run unneeded checks?)
    Sub checkForWin()
        If guessCount >= 5 Then
            If (spotOne.Text = playerMark And spotTwo.Text = playerMark And spotThree.Text = playerMark) Or (spotFour.Text = playerMark And spotFive.Text = playerMark And spotSix.Text = playerMark) Or (spotSeven.Text = playerMark And spotEight.Text = playerMark And spotNine.Text = playerMark) Or (spotOne.Text = playerMark And spotFour.Text = playerMark And spotSeven.Text = playerMark) Or (spotTwo.Text = playerMark And spotFive.Text = playerMark And spotEight.Text = playerMark) Or (spotThree.Text = playerMark And spotSix.Text = playerMark And spotNine.Text = playerMark) Or (spotOne.Text = playerMark And spotFive.Text = playerMark And spotNine.Text = playerMark) Or (spotThree.Text = playerMark And spotFive.Text = playerMark And spotSeven.Text = playerMark) Then
                lblWinner.Text = lblWinner.Text & " Player"
                disableButtons()
                resetBtn.Text = "New Game"
                winnerDetermined = True
                highlightWin()
            ElseIf (spotOne.Text = computerMark And spotTwo.Text = computerMark And spotThree.Text = computerMark) Or (spotFour.Text = computerMark And spotFive.Text = computerMark And spotSix.Text = computerMark) Or (spotSeven.Text = computerMark And spotEight.Text = computerMark And spotNine.Text = computerMark) Or (spotOne.Text = computerMark And spotFour.Text = computerMark And spotSeven.Text = computerMark) Or (spotTwo.Text = computerMark And spotFive.Text = computerMark And spotEight.Text = computerMark) Or (spotThree.Text = computerMark And spotSix.Text = computerMark And spotNine.Text = computerMark) Or (spotOne.Text = computerMark And spotFive.Text = computerMark And spotNine.Text = computerMark) Or (spotThree.Text = computerMark And spotFive.Text = computerMark And spotSeven.Text = computerMark) Then
                lblWinner.Text = lblWinner.Text & " Computer"
                disableButtons()
                resetBtn.Text = "New Game"
                winnerDetermined = True
                highlightWin()
            ElseIf guessCount = 9 Then
                lblWinner.Text = lblWinner.Text & " Draw"
                resetBtn.Text = "New Game"
                winnerDetermined = True
            End If
        End If
    End Sub

    'This highlights the way that the game has been won.
    Sub highlightWin()
        If highlightWinningTicTacToe = True Then
            If (spotOne.Text = playerMark And spotTwo.Text = playerMark And spotThree.Text = playerMark) Or (spotOne.Text = computerMark And spotTwo.Text = computerMark And spotThree.Text = computerMark) Then
                spotOne.ForeColor = colorToHighlightWinningTicTacToeWith
                spotTwo.ForeColor = colorToHighlightWinningTicTacToeWith
                spotThree.ForeColor = colorToHighlightWinningTicTacToeWith
            ElseIf (spotFour.Text = playerMark And spotFive.Text = playerMark And spotSix.Text = playerMark) Or (spotFour.Text = computerMark And spotFive.Text = computerMark And spotSix.Text = computerMark) Then
                spotFour.ForeColor = colorToHighlightWinningTicTacToeWith
                spotFive.ForeColor = colorToHighlightWinningTicTacToeWith
                spotSix.ForeColor = colorToHighlightWinningTicTacToeWith
            ElseIf (spotSeven.Text = playerMark And spotEight.Text = playerMark And spotNine.Text = playerMark) Or (spotSeven.Text = computerMark And spotEight.Text = computerMark And spotNine.Text = computerMark) Then
                spotSeven.ForeColor = colorToHighlightWinningTicTacToeWith
                spotEight.ForeColor = colorToHighlightWinningTicTacToeWith
                spotNine.ForeColor = colorToHighlightWinningTicTacToeWith
            ElseIf (spotOne.Text = playerMark And spotFour.Text = playerMark And spotSeven.Text = playerMark) Or (spotOne.Text = computerMark And spotFour.Text = computerMark And spotSeven.Text = computerMark) Then
                spotOne.ForeColor = colorToHighlightWinningTicTacToeWith
                spotFour.ForeColor = colorToHighlightWinningTicTacToeWith
                spotSeven.ForeColor = colorToHighlightWinningTicTacToeWith
            ElseIf (spotTwo.Text = playerMark And spotFive.Text = playerMark And spotEight.Text = playerMark) Or (spotTwo.Text = computerMark And spotFive.Text = computerMark And spotEight.Text = computerMark) Then
                spotTwo.ForeColor = colorToHighlightWinningTicTacToeWith
                spotFive.ForeColor = colorToHighlightWinningTicTacToeWith
                spotEight.ForeColor = colorToHighlightWinningTicTacToeWith
            ElseIf (spotThree.Text = playerMark And spotSix.Text = playerMark And spotNine.Text = playerMark) Or (spotThree.Text = computerMark And spotSix.Text = computerMark And spotNine.Text = computerMark) Then
                spotThree.ForeColor = colorToHighlightWinningTicTacToeWith
                spotSix.ForeColor = colorToHighlightWinningTicTacToeWith
                spotNine.ForeColor = colorToHighlightWinningTicTacToeWith
            ElseIf (spotOne.Text = playerMark And spotFive.Text = playerMark And spotNine.Text = playerMark) Or (spotOne.Text = computerMark And spotFive.Text = computerMark And spotNine.Text = computerMark) Then
                spotOne.ForeColor = colorToHighlightWinningTicTacToeWith
                spotFive.ForeColor = colorToHighlightWinningTicTacToeWith
                spotNine.ForeColor = colorToHighlightWinningTicTacToeWith
            ElseIf (spotThree.Text = playerMark And spotFive.Text = playerMark And spotSeven.Text = playerMark) Or (spotThree.Text = computerMark And spotFive.Text = computerMark And spotSeven.Text = computerMark) Then
                spotThree.ForeColor = colorToHighlightWinningTicTacToeWith
                spotFive.ForeColor = colorToHighlightWinningTicTacToeWith
                spotSeven.ForeColor = colorToHighlightWinningTicTacToeWith
            End If
        End If
    End Sub

    'To prevent players from continuing the game after
    'a winner has already been chosen, all of the buttons
    'will be deactivated using this.
    Sub disableButtons()
        spotOneGuessed = True
        spotTwoGuessed = True
        spotThreeGuessed = True
        spotFourGuessed = True
        spotFiveGuessed = True
        spotSixGuessed = True
        spotSevenGuessed = True
        spotEightGuessed = True
        spotNineGuessed = True
    End Sub

    'Here is where the action occurs with the users...
    'The buttons are clicked, and they react!
    Private Sub spotOne_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles spotOne.Click
        If spotOneGuessed = False Then
            spotOne.Text = playerMark
            guessCount += 1
            spotOneGuessed = True
            resetBtn.Focus()
            checkForWin()
            computerGuess()
        End If
    End Sub
    Private Sub spotTwo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles spotTwo.Click
        If spotTwoGuessed = False Then
            spotTwo.Text = playerMark
            guessCount += 1
            spotTwoGuessed = True
            resetBtn.Focus()
            checkForWin()
            computerGuess()
        End If
    End Sub
    Private Sub spotThree_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles spotThree.Click
        If spotThreeGuessed = False Then
            spotThree.Text = playerMark
            guessCount += 1
            spotThreeGuessed = True
            resetBtn.Focus()
            checkForWin()
            computerGuess()
        End If
    End Sub
    Private Sub spotFour_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles spotFour.Click
        If spotFourGuessed = False Then
            spotFour.Text = playerMark
            guessCount += 1
            spotFourGuessed = True
            resetBtn.Focus()
            checkForWin()
            computerGuess()
        End If
    End Sub
    Private Sub spotFive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles spotFive.Click
        If spotFiveGuessed = False Then
            spotFive.Text = playerMark
            guessCount += 1
            spotFiveGuessed = True
            resetBtn.Focus()
            checkForWin()
            computerGuess()
        End If
    End Sub
    Private Sub spotSix_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles spotSix.Click
        If spotSixGuessed = False Then
            spotSix.Text = playerMark
            guessCount += 1
            spotSixGuessed = True
            resetBtn.Focus()
            checkForWin()
            computerGuess()
        End If
    End Sub
    Private Sub spotSeven_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles spotSeven.Click
        If spotSevenGuessed = False Then
            spotSeven.Text = playerMark
            guessCount += 1
            spotSevenGuessed = True
            resetBtn.Focus()
            checkForWin()
            computerGuess()
        End If
    End Sub
    Private Sub spotEight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles spotEight.Click
        If spotEightGuessed = False Then
            spotEight.Text = playerMark
            guessCount += 1
            spotEightGuessed = True
            resetBtn.Focus()
            checkForWin()
            computerGuess()
        End If
    End Sub
    Private Sub spotNine_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles spotNine.Click
        If spotNineGuessed = False Then
            spotNine.Text = playerMark
            guessCount += 1
            spotNineGuessed = True
            resetBtn.Focus()
            checkForWin()
            computerGuess()
        End If
    End Sub

    'This makes the settings button work.
    Private Sub btnSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSettings.Click
        showSettingsElements()
    End Sub

    'This will reverse the marks used by both, the user
    'and the computer.
    Sub reversePlayerComputerMarks()
        If spotOne.Text = computerMark Then
            spotOne.Text = playerMark
        ElseIf spotOne.Text = playerMark Then
            spotOne.Text = computerMark
        End If
        If spotTwo.Text = computerMark Then
            spotTwo.Text = playerMark
        ElseIf spotTwo.Text = playerMark Then
            spotTwo.Text = computerMark
        End If
        If spotThree.Text = computerMark Then
            spotThree.Text = playerMark
        ElseIf spotThree.Text = playerMark Then
            spotThree.Text = computerMark
        End If
        If spotFour.Text = computerMark Then
            spotFour.Text = playerMark
        ElseIf spotFour.Text = playerMark Then
            spotFour.Text = computerMark
        End If
        If spotFive.Text = computerMark Then
            spotFive.Text = playerMark
        ElseIf spotFive.Text = playerMark Then
            spotFive.Text = computerMark
        End If
        If spotSix.Text = computerMark Then
            spotSix.Text = playerMark
        ElseIf spotSix.Text = playerMark Then
            spotSix.Text = computerMark
        End If
        If spotSeven.Text = computerMark Then
            spotSeven.Text = playerMark
        ElseIf spotSeven.Text = playerMark Then
            spotSeven.Text = computerMark
        End If
        If spotEight.Text = computerMark Then
            spotEight.Text = playerMark
        ElseIf spotEight.Text = playerMark Then
            spotEight.Text = computerMark
        End If
        If spotNine.Text = computerMark Then
            spotNine.Text = playerMark
        ElseIf spotNine.Text = playerMark Then
            spotNine.Text = computerMark
        End If
    End Sub

    'This makes the settings screen match up with the variables that are in the program.
    Private Sub matchSettingsToVariables()
        If playerMark = "X" Then
            radbtnPlayerMarkX.Checked = True
        ElseIf playerMark = "O" Then
            radbtnPlayerMarkO.Checked = True
        End If
        If highlightWinningTicTacToe = True Then
            radbtnHighlightWinning3InARowYes.Checked = True
        ElseIf highlightWinningTicTacToe = False Then
            radbtnHighlightWinning3InARowNo.Checked = True
        End If
        If colorToHighlightWinningTicTacToeWith = Color.Red Then
            drpWinning3InARowColor.Text = "Red"
        ElseIf colorToHighlightWinningTicTacToeWith = Color.Orange Then
            drpWinning3InARowColor.Text = "Orange"
        ElseIf colorToHighlightWinningTicTacToeWith = Color.Yellow Then
            drpWinning3InARowColor.Text = "Yellow"
        ElseIf colorToHighlightWinningTicTacToeWith = Color.Green Then
            drpWinning3InARowColor.Text = "Green"
        ElseIf colorToHighlightWinningTicTacToeWith = Color.Blue Then
            drpWinning3InARowColor.Text = "Blue"
        ElseIf colorToHighlightWinningTicTacToeWith = Color.Purple Then
            drpWinning3InARowColor.Text = "Purple"
        ElseIf colorToHighlightWinningTicTacToeWith = Color.Pink Then
            drpWinning3InARowColor.Text = "Pink"
        End If
    End Sub

    'This makes all elements of the Tic-Tac-Toe game visible, and all elements of the settings page invisible.
    Sub showTicTacToeElements()
        lblSettings.Visible = False
        grpPlayerMark.Visible = False
        grpHighlightWinning3InARow.Visible = False
        grpHighlightWinColor.Visible = False
        btnSaveSettings.Visible = False
        btnCancelSettings.Visible = False
        lblVersion.Visible = False
        spotOne.Visible = True
        spotTwo.Visible = True
        spotThree.Visible = True
        spotFour.Visible = True
        spotFive.Visible = True
        spotSix.Visible = True
        spotSeven.Visible = True
        spotEight.Visible = True
        spotNine.Visible = True
        btnSettings.Visible = True
        resetBtn.Visible = True
        lblWinner.Visible = True
        btnBugReport.Visible = False 'This line was added in Tic-Tac-Toe v4.0
    End Sub

    'This makes all elements of the settings page visible, and all elements of the Tic-Tac-Toe game invisible.
    Sub showSettingsElements()
        spotOne.Visible = False
        spotTwo.Visible = False
        spotThree.Visible = False
        spotFour.Visible = False
        spotFive.Visible = False
        spotSix.Visible = False
        spotSeven.Visible = False
        spotEight.Visible = False
        spotNine.Visible = False
        btnSettings.Visible = False
        resetBtn.Visible = False
        lblWinner.Visible = False
        lblSettings.Visible = True
        grpPlayerMark.Visible = True
        grpHighlightWinning3InARow.Visible = True
        grpHighlightWinColor.Visible = True
        btnSaveSettings.Visible = True
        btnCancelSettings.Visible = True
        lblVersion.Visible = True
        btnBugReport.Visible = True 'This line added in Tic-Tac-Toe v4.0.
    End Sub

    'This will save changes made to the program settings.
    Private Sub btnSaveSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSettings.Click
        'Determine the mark which the player would like to use.
        If radbtnPlayerMarkX.Checked = True Then
            playerMark = "X"
            computerMark = "O"
        ElseIf radbtnPlayerMarkO.Checked = True Then
            playerMark = "O"
            computerMark = "X"
        End If
        reversePlayerComputerMarks() 'This just makes sure that the mark in already marked buttons is replaced.
        'This will set the variable that is responsible for determining whether or not the winning 3-in-a row
        'is highlighted.
        If radbtnHighlightWinning3InARowYes.Checked = True Then
            highlightWinningTicTacToe = True
        Else
            highlightWinningTicTacToe = False
        End If
        'This will set the string variable that holds the color that is used when
        'highlighting the winning 3-in-a-row.
        If drpWinning3InARowColor.Text = "Red" Then
            colorToHighlightWinningTicTacToeWith = Color.Red
        ElseIf drpWinning3InARowColor.Text = "Orange" Then
            colorToHighlightWinningTicTacToeWith = Color.Orange
        ElseIf drpWinning3InARowColor.Text = "Yellow" Then
            colorToHighlightWinningTicTacToeWith = Color.Yellow
        ElseIf drpWinning3InARowColor.Text = "Green" Then
            colorToHighlightWinningTicTacToeWith = Color.Green
        ElseIf drpWinning3InARowColor.Text = "Blue" Then
            colorToHighlightWinningTicTacToeWith = Color.Blue
        ElseIf drpWinning3InARowColor.Text = "Purple" Then
            colorToHighlightWinningTicTacToeWith = Color.Purple
        ElseIf drpWinning3InARowColor.Text = "Pink" Then
            colorToHighlightWinningTicTacToeWith = Color.Pink
        Else
            If colorToHighlightWinningTicTacToeWith = Color.Red Then
                drpWinning3InARowColor.Text = "Red"
            ElseIf colorToHighlightWinningTicTacToeWith = Color.Orange Then
                drpWinning3InARowColor.Text = "Orange"
            ElseIf colorToHighlightWinningTicTacToeWith = Color.Yellow Then
                drpWinning3InARowColor.Text = "Yellow"
            ElseIf colorToHighlightWinningTicTacToeWith = Color.Green Then
                drpWinning3InARowColor.Text = "Green"
            ElseIf colorToHighlightWinningTicTacToeWith = Color.Blue Then
                drpWinning3InARowColor.Text = "Blue"
            ElseIf colorToHighlightWinningTicTacToeWith = Color.Purple Then
                drpWinning3InARowColor.Text = "Purple"
            ElseIf colorToHighlightWinningTicTacToeWith = Color.Pink Then
                drpWinning3InARowColor.Text = "Pink"
            End If
        End If
        'All variables have been changed accordingly to match the new settings, now write the changes to the text file.
        writeSettingsToFile()
        'Now, return to the Tic-Tac-Toe board.
        showTicTacToeElements()
    End Sub

    'This will Cancel changes to the program settings.
    Private Sub btnCancelSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelSettings.Click
        showTicTacToeElements()
        matchSettingsToVariables()
    End Sub

    'This makes the reset button work!
    Private Sub resetBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles resetBtn.Click
        resetBtn.Text = "Reset"
        lblWinner.Text = "Winner:"
        winnerDetermined = False
        guessCount = 0
        spotOne.Text = ""
        spotTwo.Text = ""
        spotThree.Text = ""
        spotFour.Text = ""
        spotFive.Text = ""
        spotSix.Text = ""
        spotSeven.Text = ""
        spotEight.Text = ""
        spotNine.Text = ""
        spotOneGuessed = False
        spotTwoGuessed = False
        spotThreeGuessed = False
        spotFourGuessed = False
        spotFiveGuessed = False
        spotSixGuessed = False
        spotSevenGuessed = False
        spotEightGuessed = False
        spotNineGuessed = False
        spotOne.Enabled = True
        spotTwo.Enabled = True
        spotThree.Enabled = True
        spotFour.Enabled = True
        spotFive.Enabled = True
        spotSix.Enabled = True
        spotSeven.Enabled = True
        spotEight.Enabled = True
        spotNine.Enabled = True
        spotOne.ForeColor = Color.Black
        spotTwo.ForeColor = Color.Black
        spotThree.ForeColor = Color.Black
        spotFour.ForeColor = Color.Black
        spotFive.ForeColor = Color.Black
        spotSix.ForeColor = Color.Black
        spotSeven.ForeColor = Color.Black
        spotEight.ForeColor = Color.Black
        spotNine.ForeColor = Color.Black
    End Sub

    'This does not do anything other than open up the bug reporting form.
    Private Sub btnBugReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBugReport.Click
        frmBugs.Show()
    End Sub
End Class

'Program Information Log
'
'Tic-Tac-Toe v1.0 -- Release Date: Sunday, November 25, 2012 @ 2:05:29 PM
' ~ New Features
'  - This is the first version of the game, so all of it is "new."
'  - "Switch To O/X" button.
'  - "Clear/New Game" button.
'  - "Winner" label.
'  - Player is versing the computer.
'  - Computer is randomly selecting spots on the board, in
'    no particular order.
' ~ Bug Fixes
'  - This is the first version of the game, so there has not
'    been sufficient time to find bugs.
' ~ Coming Soon (Planned Features for Future Versions)
'  - Player vs. Player
'  - Randomization of who starts game in Player vs. Computer.
'  - Randomization of who is initially X and who is initially O
'    in Player vs. Computer.
'  - Implementation of logic to make it easier for the computer
'    to "beat" the user. (And make the game more challenging for
'    the user.)
'  - (Being Considered, Not Definite) Slowing of computer decision
'    regarding where to place its mark. This would make the game
'    more suspensful and realistic for the player.
'  - (Being Considered, Not Definite) Change of program interface.
'
'----------------------------------------------------------------
'
'Tic-Tac-Toe v2.0 -- Release Date: Monday, December 10, 2012 @ 9:37:37 PM
' ~ New Features
'  - Computer no longer relies totally on randomization for picking spots.
'    It now uses various conditionals along with randomization for picking spots.
'  - Instead of always starting out with the player as X and the computer as O,
'    the program now randomizes who is initially what at program start.
'  - Tab index now runs in a logical order.
'  - Instead of the focus being randomly being assigned to the next available
'    enabled button in the program, the focus is now given back to the "Reset"
'    button after the player chooses a spot.
' ~ Known Bugs
'  - No known bugs have been found in v2.0 as of yet.
' ~ Bug Fixes
'  - Since v1.0, not bugs have been found.
' ~ Coming Soon (Planned Features for Future Versions)
'  - Highlighting of winner marks.
'  - Randomizaton of who is initially X and who is initially O upon program start-up.
'  - New User Interface (? -- If included, the randomization feature will likely be scrapped.)
'  - New Option Panel (?)
'  - All "Coming Soon" features that were suggested in the v1.0 program log that
'    were not implemented in v2.0.
'
'----------------------------------------------------------------
'
'Tic-Tac-Toe v3.0 -- Release Date: In Development; Not Released Yet
' ~ New Features
'  - Restore Button Has Been Disabled
'  - Program Can No Longer Be Resized
'  - "Switch To O/X" button retired.
'  - New "Settings" button added.
'  - New "Setting" interface.
'  - Program Settings are now stored in a text file for use for each time the program runs.
'  - Buttons no longer rely on deactivation to make sure that they are not clicked again by the user.
'    Instead, they now use if statements for each button to void any second attempts at clicking the button. The
'    variables that they use to work were declared in Tic-Tac-Toe v1.0 for use elsewhere.
'  - Winning 3-in-a-row is highlighted, option to disable this feature has been added alongside it, with the
'    ability to change the color which the winning 3-in-a-row is highlighted with.
' ~ Known Bugs
'  - No known bugs have been found in v3.0 as of yet.
' ~ Bug Fixes
'  - Since v1.0, no bugs have been found.
' ~ Coming Soon (Planned Features for Future Versions)
'  - Coming Soon -- So far, nothing.
'
'----------------------------------------------------------------
'
'Tic-Tac-Toe v4.0 -- Release Date: In Development; Not Released Yet
' ~ New Features
'  - Tic-Tac-Toe Settings File directory changed. The Tic-Tac-Toe Settings File is now stored in the now-standard
'    'ProgrammersWorldProgramFiles' folder.
'  - Auto-Import system added, which automatically imports Tic-Tac-Toe v3.0 Settings File from their old location,
'    and updates them to match with the v4.0 settings file formatting.
'  - Bug Reporting System Added -- The new bug reporting system makes use of your Programmer's World account. It
'    opens in a separate window.
' ~ Known Bugs
'  - No known bugs have been found in v4.0 as of yet.
' ~ Bug Fixes
'  - Since v1.0, no bugs have been found.
' ~ Coming Soon (Planned Features for Future Versions)
'  - Coming Soon -- So far, nothing.