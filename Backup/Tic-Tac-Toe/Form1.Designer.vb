﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TicTacToe
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.spotOne = New System.Windows.Forms.Button
        Me.spotFive = New System.Windows.Forms.Button
        Me.spotTwo = New System.Windows.Forms.Button
        Me.spotSix = New System.Windows.Forms.Button
        Me.spotThree = New System.Windows.Forms.Button
        Me.spotFour = New System.Windows.Forms.Button
        Me.spotSeven = New System.Windows.Forms.Button
        Me.spotEight = New System.Windows.Forms.Button
        Me.spotNine = New System.Windows.Forms.Button
        Me.resetBtn = New System.Windows.Forms.Button
        Me.btnSettings = New System.Windows.Forms.Button
        Me.lblWinner = New System.Windows.Forms.Label
        Me.lblSettings = New System.Windows.Forms.Label
        Me.radbtnPlayerMarkX = New System.Windows.Forms.RadioButton
        Me.radbtnPlayerMarkO = New System.Windows.Forms.RadioButton
        Me.grpPlayerMark = New System.Windows.Forms.GroupBox
        Me.lblVersion = New System.Windows.Forms.Label
        Me.btnSaveSettings = New System.Windows.Forms.Button
        Me.btnCancelSettings = New System.Windows.Forms.Button
        Me.drpWinning3InARowColor = New System.Windows.Forms.ComboBox
        Me.grpHighlightWinColor = New System.Windows.Forms.GroupBox
        Me.grpHighlightWinning3InARow = New System.Windows.Forms.GroupBox
        Me.radbtnHighlightWinning3InARowYes = New System.Windows.Forms.RadioButton
        Me.radbtnHighlightWinning3InARowNo = New System.Windows.Forms.RadioButton
        Me.btnBugReport = New System.Windows.Forms.Button
        Me.grpPlayerMark.SuspendLayout()
        Me.grpHighlightWinColor.SuspendLayout()
        Me.grpHighlightWinning3InARow.SuspendLayout()
        Me.SuspendLayout()
        '
        'spotOne
        '
        Me.spotOne.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spotOne.Location = New System.Drawing.Point(12, 9)
        Me.spotOne.Name = "spotOne"
        Me.spotOne.Size = New System.Drawing.Size(75, 75)
        Me.spotOne.TabIndex = 3
        Me.spotOne.UseVisualStyleBackColor = True
        '
        'spotFive
        '
        Me.spotFive.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spotFive.Location = New System.Drawing.Point(93, 90)
        Me.spotFive.Name = "spotFive"
        Me.spotFive.Size = New System.Drawing.Size(75, 75)
        Me.spotFive.TabIndex = 7
        Me.spotFive.UseVisualStyleBackColor = True
        '
        'spotTwo
        '
        Me.spotTwo.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spotTwo.Location = New System.Drawing.Point(93, 9)
        Me.spotTwo.Name = "spotTwo"
        Me.spotTwo.Size = New System.Drawing.Size(75, 75)
        Me.spotTwo.TabIndex = 4
        Me.spotTwo.UseVisualStyleBackColor = True
        '
        'spotSix
        '
        Me.spotSix.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spotSix.Location = New System.Drawing.Point(174, 90)
        Me.spotSix.Name = "spotSix"
        Me.spotSix.Size = New System.Drawing.Size(75, 75)
        Me.spotSix.TabIndex = 8
        Me.spotSix.UseVisualStyleBackColor = True
        '
        'spotThree
        '
        Me.spotThree.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spotThree.Location = New System.Drawing.Point(174, 9)
        Me.spotThree.Name = "spotThree"
        Me.spotThree.Size = New System.Drawing.Size(75, 75)
        Me.spotThree.TabIndex = 5
        Me.spotThree.UseVisualStyleBackColor = True
        '
        'spotFour
        '
        Me.spotFour.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spotFour.Location = New System.Drawing.Point(12, 90)
        Me.spotFour.Name = "spotFour"
        Me.spotFour.Size = New System.Drawing.Size(75, 75)
        Me.spotFour.TabIndex = 6
        Me.spotFour.UseVisualStyleBackColor = True
        '
        'spotSeven
        '
        Me.spotSeven.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spotSeven.Location = New System.Drawing.Point(12, 171)
        Me.spotSeven.Name = "spotSeven"
        Me.spotSeven.Size = New System.Drawing.Size(75, 75)
        Me.spotSeven.TabIndex = 9
        Me.spotSeven.UseVisualStyleBackColor = True
        '
        'spotEight
        '
        Me.spotEight.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spotEight.Location = New System.Drawing.Point(93, 171)
        Me.spotEight.Name = "spotEight"
        Me.spotEight.Size = New System.Drawing.Size(75, 75)
        Me.spotEight.TabIndex = 10
        Me.spotEight.UseVisualStyleBackColor = True
        '
        'spotNine
        '
        Me.spotNine.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spotNine.Location = New System.Drawing.Point(174, 171)
        Me.spotNine.Name = "spotNine"
        Me.spotNine.Size = New System.Drawing.Size(75, 75)
        Me.spotNine.TabIndex = 11
        Me.spotNine.UseVisualStyleBackColor = True
        '
        'resetBtn
        '
        Me.resetBtn.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.resetBtn.Location = New System.Drawing.Point(12, 283)
        Me.resetBtn.Name = "resetBtn"
        Me.resetBtn.Size = New System.Drawing.Size(238, 23)
        Me.resetBtn.TabIndex = 1
        Me.resetBtn.Text = "Reset"
        Me.resetBtn.UseVisualStyleBackColor = True
        '
        'btnSettings
        '
        Me.btnSettings.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSettings.Location = New System.Drawing.Point(12, 252)
        Me.btnSettings.Name = "btnSettings"
        Me.btnSettings.Size = New System.Drawing.Size(238, 23)
        Me.btnSettings.TabIndex = 2
        Me.btnSettings.Text = "Settings"
        Me.btnSettings.UseVisualStyleBackColor = True
        '
        'lblWinner
        '
        Me.lblWinner.AutoSize = True
        Me.lblWinner.Location = New System.Drawing.Point(12, 314)
        Me.lblWinner.Name = "lblWinner"
        Me.lblWinner.Size = New System.Drawing.Size(44, 13)
        Me.lblWinner.TabIndex = 0
        Me.lblWinner.Text = "Winner:"
        '
        'lblSettings
        '
        Me.lblSettings.AutoSize = True
        Me.lblSettings.Location = New System.Drawing.Point(88, 10)
        Me.lblSettings.Name = "lblSettings"
        Me.lblSettings.Size = New System.Drawing.Size(87, 13)
        Me.lblSettings.TabIndex = 12
        Me.lblSettings.Text = "Program Settings"
        Me.lblSettings.Visible = False
        '
        'radbtnPlayerMarkX
        '
        Me.radbtnPlayerMarkX.AutoSize = True
        Me.radbtnPlayerMarkX.Checked = True
        Me.radbtnPlayerMarkX.Location = New System.Drawing.Point(6, 19)
        Me.radbtnPlayerMarkX.Name = "radbtnPlayerMarkX"
        Me.radbtnPlayerMarkX.Size = New System.Drawing.Size(32, 17)
        Me.radbtnPlayerMarkX.TabIndex = 14
        Me.radbtnPlayerMarkX.TabStop = True
        Me.radbtnPlayerMarkX.Text = "X"
        Me.radbtnPlayerMarkX.UseVisualStyleBackColor = True
        '
        'radbtnPlayerMarkO
        '
        Me.radbtnPlayerMarkO.AutoSize = True
        Me.radbtnPlayerMarkO.Location = New System.Drawing.Point(44, 19)
        Me.radbtnPlayerMarkO.Name = "radbtnPlayerMarkO"
        Me.radbtnPlayerMarkO.Size = New System.Drawing.Size(33, 17)
        Me.radbtnPlayerMarkO.TabIndex = 15
        Me.radbtnPlayerMarkO.Text = "O"
        Me.radbtnPlayerMarkO.UseVisualStyleBackColor = True
        '
        'grpPlayerMark
        '
        Me.grpPlayerMark.Controls.Add(Me.radbtnPlayerMarkX)
        Me.grpPlayerMark.Controls.Add(Me.radbtnPlayerMarkO)
        Me.grpPlayerMark.Location = New System.Drawing.Point(12, 26)
        Me.grpPlayerMark.Name = "grpPlayerMark"
        Me.grpPlayerMark.Size = New System.Drawing.Size(237, 41)
        Me.grpPlayerMark.TabIndex = 16
        Me.grpPlayerMark.TabStop = False
        Me.grpPlayerMark.Text = "Player Mark"
        Me.grpPlayerMark.Visible = False
        '
        'lblVersion
        '
        Me.lblVersion.AutoSize = True
        Me.lblVersion.Location = New System.Drawing.Point(12, 314)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(186, 13)
        Me.lblVersion.TabIndex = 17
        Me.lblVersion.Text = "Version Information: Tic-Tac-Toe v4.0"
        Me.lblVersion.Visible = False
        '
        'btnSaveSettings
        '
        Me.btnSaveSettings.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSettings.Location = New System.Drawing.Point(12, 252)
        Me.btnSaveSettings.Name = "btnSaveSettings"
        Me.btnSaveSettings.Size = New System.Drawing.Size(238, 23)
        Me.btnSaveSettings.TabIndex = 19
        Me.btnSaveSettings.Text = "OK"
        Me.btnSaveSettings.UseVisualStyleBackColor = True
        Me.btnSaveSettings.Visible = False
        '
        'btnCancelSettings
        '
        Me.btnCancelSettings.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelSettings.Location = New System.Drawing.Point(12, 283)
        Me.btnCancelSettings.Name = "btnCancelSettings"
        Me.btnCancelSettings.Size = New System.Drawing.Size(238, 23)
        Me.btnCancelSettings.TabIndex = 18
        Me.btnCancelSettings.Text = "Cancel"
        Me.btnCancelSettings.UseVisualStyleBackColor = True
        Me.btnCancelSettings.Visible = False
        '
        'drpWinning3InARowColor
        '
        Me.drpWinning3InARowColor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.drpWinning3InARowColor.FormattingEnabled = True
        Me.drpWinning3InARowColor.Items.AddRange(New Object() {"Red", "Orange", "Yellow", "Green", "Blue", "Purple", "Pink"})
        Me.drpWinning3InARowColor.Location = New System.Drawing.Point(6, 19)
        Me.drpWinning3InARowColor.Name = "drpWinning3InARowColor"
        Me.drpWinning3InARowColor.Size = New System.Drawing.Size(121, 21)
        Me.drpWinning3InARowColor.TabIndex = 20
        Me.drpWinning3InARowColor.Text = "Red"
        '
        'grpHighlightWinColor
        '
        Me.grpHighlightWinColor.Controls.Add(Me.drpWinning3InARowColor)
        Me.grpHighlightWinColor.Location = New System.Drawing.Point(13, 116)
        Me.grpHighlightWinColor.Name = "grpHighlightWinColor"
        Me.grpHighlightWinColor.Size = New System.Drawing.Size(237, 49)
        Me.grpHighlightWinColor.TabIndex = 21
        Me.grpHighlightWinColor.TabStop = False
        Me.grpHighlightWinColor.Text = "Color to Highlight Winning 3-In-A-Row With"
        Me.grpHighlightWinColor.Visible = False
        '
        'grpHighlightWinning3InARow
        '
        Me.grpHighlightWinning3InARow.Controls.Add(Me.radbtnHighlightWinning3InARowYes)
        Me.grpHighlightWinning3InARow.Controls.Add(Me.radbtnHighlightWinning3InARowNo)
        Me.grpHighlightWinning3InARow.Location = New System.Drawing.Point(12, 73)
        Me.grpHighlightWinning3InARow.Name = "grpHighlightWinning3InARow"
        Me.grpHighlightWinning3InARow.Size = New System.Drawing.Size(237, 41)
        Me.grpHighlightWinning3InARow.TabIndex = 22
        Me.grpHighlightWinning3InARow.TabStop = False
        Me.grpHighlightWinning3InARow.Text = "Highlight Winning 3-In-A-Row"
        Me.grpHighlightWinning3InARow.Visible = False
        '
        'radbtnHighlightWinning3InARowYes
        '
        Me.radbtnHighlightWinning3InARowYes.AutoSize = True
        Me.radbtnHighlightWinning3InARowYes.Checked = True
        Me.radbtnHighlightWinning3InARowYes.Location = New System.Drawing.Point(6, 19)
        Me.radbtnHighlightWinning3InARowYes.Name = "radbtnHighlightWinning3InARowYes"
        Me.radbtnHighlightWinning3InARowYes.Size = New System.Drawing.Size(43, 17)
        Me.radbtnHighlightWinning3InARowYes.TabIndex = 14
        Me.radbtnHighlightWinning3InARowYes.TabStop = True
        Me.radbtnHighlightWinning3InARowYes.Text = "Yes"
        Me.radbtnHighlightWinning3InARowYes.UseVisualStyleBackColor = True
        '
        'radbtnHighlightWinning3InARowNo
        '
        Me.radbtnHighlightWinning3InARowNo.AutoSize = True
        Me.radbtnHighlightWinning3InARowNo.Location = New System.Drawing.Point(55, 19)
        Me.radbtnHighlightWinning3InARowNo.Name = "radbtnHighlightWinning3InARowNo"
        Me.radbtnHighlightWinning3InARowNo.Size = New System.Drawing.Size(39, 17)
        Me.radbtnHighlightWinning3InARowNo.TabIndex = 15
        Me.radbtnHighlightWinning3InARowNo.Text = "No"
        Me.radbtnHighlightWinning3InARowNo.UseVisualStyleBackColor = True
        '
        'btnBugReport
        '
        Me.btnBugReport.Location = New System.Drawing.Point(12, 171)
        Me.btnBugReport.Name = "btnBugReport"
        Me.btnBugReport.Size = New System.Drawing.Size(238, 23)
        Me.btnBugReport.TabIndex = 21
        Me.btnBugReport.Text = "Submit A Bug Report"
        Me.btnBugReport.UseVisualStyleBackColor = True
        Me.btnBugReport.Visible = False
        '
        'TicTacToe
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(262, 336)
        Me.Controls.Add(Me.btnBugReport)
        Me.Controls.Add(Me.grpHighlightWinning3InARow)
        Me.Controls.Add(Me.grpHighlightWinColor)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.btnSaveSettings)
        Me.Controls.Add(Me.btnCancelSettings)
        Me.Controls.Add(Me.grpPlayerMark)
        Me.Controls.Add(Me.lblSettings)
        Me.Controls.Add(Me.lblWinner)
        Me.Controls.Add(Me.btnSettings)
        Me.Controls.Add(Me.resetBtn)
        Me.Controls.Add(Me.spotNine)
        Me.Controls.Add(Me.spotEight)
        Me.Controls.Add(Me.spotSeven)
        Me.Controls.Add(Me.spotFour)
        Me.Controls.Add(Me.spotThree)
        Me.Controls.Add(Me.spotSix)
        Me.Controls.Add(Me.spotTwo)
        Me.Controls.Add(Me.spotFive)
        Me.Controls.Add(Me.spotOne)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(270, 373)
        Me.MinimumSize = New System.Drawing.Size(270, 363)
        Me.Name = "TicTacToe"
        Me.Text = "Tic-Tac-Toe v4.0"
        Me.grpPlayerMark.ResumeLayout(False)
        Me.grpPlayerMark.PerformLayout()
        Me.grpHighlightWinColor.ResumeLayout(False)
        Me.grpHighlightWinning3InARow.ResumeLayout(False)
        Me.grpHighlightWinning3InARow.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents spotOne As System.Windows.Forms.Button
    Friend WithEvents spotFive As System.Windows.Forms.Button
    Friend WithEvents spotTwo As System.Windows.Forms.Button
    Friend WithEvents spotSix As System.Windows.Forms.Button
    Friend WithEvents spotThree As System.Windows.Forms.Button
    Friend WithEvents spotFour As System.Windows.Forms.Button
    Friend WithEvents spotSeven As System.Windows.Forms.Button
    Friend WithEvents spotEight As System.Windows.Forms.Button
    Friend WithEvents spotNine As System.Windows.Forms.Button
    Friend WithEvents resetBtn As System.Windows.Forms.Button
    Friend WithEvents btnSettings As System.Windows.Forms.Button
    Friend WithEvents lblWinner As System.Windows.Forms.Label
    Friend WithEvents lblSettings As System.Windows.Forms.Label
    Friend WithEvents radbtnPlayerMarkX As System.Windows.Forms.RadioButton
    Friend WithEvents radbtnPlayerMarkO As System.Windows.Forms.RadioButton
    Friend WithEvents grpPlayerMark As System.Windows.Forms.GroupBox
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents btnSaveSettings As System.Windows.Forms.Button
    Friend WithEvents btnCancelSettings As System.Windows.Forms.Button
    Friend WithEvents drpWinning3InARowColor As System.Windows.Forms.ComboBox
    Friend WithEvents grpHighlightWinColor As System.Windows.Forms.GroupBox
    Friend WithEvents grpHighlightWinning3InARow As System.Windows.Forms.GroupBox
    Friend WithEvents radbtnHighlightWinning3InARowYes As System.Windows.Forms.RadioButton
    Friend WithEvents radbtnHighlightWinning3InARowNo As System.Windows.Forms.RadioButton
    Friend WithEvents btnBugReport As System.Windows.Forms.Button
End Class
