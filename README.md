# Tic-Tac-Toe #

This program allows the user to play Tic-Tac-Toe with the computer. Written in Visual Basic, this program is several years old and is no longer in active development. Derivative works are permitted provided that the original source be credited (MIT License). The earliest binaries of this project date back to 2012, with the last, incomplete version dating back to 2013.

**This was one of my first computer programming projects, as is obvious by the sloppiness of the code. I make this code publicly available with the hope that it will be beneficial to someone. Feel free to tidy up the code and redistribute it.**

## I don't want the code. Where can I get just the game? ##

Downloads for each version of Tic-Tac-Toe are available on the [repository download page](https://bitbucket.org/David-B/tic-tac-toe/downloads). The recommended version to try is Tic-Tac-Toe v3.0 because it was the last version of the game that I fully finished developing.

## Redistribution License (MIT) ##

The MIT License (MIT)

Copyright (c) 2015 David Berdik

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.